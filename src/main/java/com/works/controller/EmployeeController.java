package com.works.controller;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.works.exception.EmployeeNotFoundException;
import com.works.model.Employee;
import com.works.repository.EmployeeRepository;

@RestController
@RequestMapping(value = "/employee-management", produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated
public class EmployeeController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private EmployeeRepository repository;

	public EmployeeRepository getRepository() {
		return repository;
	}

	public void setRepository(EmployeeRepository repository) {
		this.repository = repository;
	}

	@GetMapping(value = "/employees")
	public List<Employee> getAllEmployees() {
        logger.info("Getting employee details from the database.");
		return repository.findAll();
	}

	@PostMapping("/employees")
	Employee createOrSaveEmployee(@RequestBody Employee newEmployee) {
        logger.info("Saving employee details in the database.");
		return repository.save(newEmployee);
	}
	
	@GetMapping("/employees/{id}")
	Employee getEmployeeById(@PathVariable 
							 @Min(value = 1, message = "id must be greater than or equal to 1") 
							 @Max(value = 1000, message = "id must be lower than or equal to 1000") Long id)
	{
        logger.info("Getting employee details w.r.t id");
	    return repository.findById(id)
	            .orElseThrow(() -> new EmployeeNotFoundException("Employee id '" + id + "' does no exist"));
	}

	@PutMapping("/employees/{id}")
	Employee updateEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) {
        logger.info("Updating employee details in the database.");


		return repository.findById(id).map(employee -> {
            mappingValues(newEmployee, employee);
            return repository.save(employee);
		}).orElseGet(() -> {
			newEmployee.setId(id);
			return repository.save(newEmployee);
		});
	}



    @DeleteMapping("/employees/{id}")
	void deleteEmployee(@PathVariable Long id) {
        logger.info("Deleting  employee details from the database. w.r.t "+id);
		repository.deleteById(id);
	}
    private void mappingValues(@RequestBody Employee newEmployee, Employee employee) {
        employee.setFirstName(newEmployee.getFirstName());
        employee.setLastName(newEmployee.getLastName());
        employee.setEmail(newEmployee.getEmail());
        employee.setAddress(newEmployee.getAddress());
        employee.setDate_of_birth(newEmployee.getDate_of_birth());
        employee.setSalary(newEmployee.getSalary());
    }
}